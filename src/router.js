import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/components/Home';
import Datasets from '@/components/Datasets';
import Catalogues from '@/components/Catalogues';
import DatasetDetails from '@/components/EDP2-datasetDetails';
import Imprint from '@/components/Imprint';
import MapBasic from '@/components/MapBasic';
import NotFound from '@/components/NotFound';
// import DistributionDetails from '@/components/DistributionDetails';

import GLUE_CONFIG from '../user-config/glue-config';

Vue.use(Router);

const title = GLUE_CONFIG.title || 'Viaduct UI';

// Documentation for Vue Router options -> https://router.vuejs.org/api/#routes
const router = new Router({
  base: GLUE_CONFIG.routerOptions.base,
  mode: GLUE_CONFIG.routerOptions.mode,
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: '/datasets',
      meta: {
        title,
      },
    },
    {
      path: '/datasets',
      name: 'Datasets',
      component: Datasets,
      meta: {
        title,
      },
      // props: { infiniteScrolling: false, pagination: true },
    },
    {
      path: '/datasets/:ds_id',
      name: 'DatasetDetails',
      component: DatasetDetails,
      props: {
        activeTab: 0,
      },
      meta: {
        title,
      },
    },
    {
      path: '/datasets/:ds_id/categories',
      name: 'DatasetDetailsCategories',
      component: DatasetDetails,
      props: {
        activeTab: 1,
      },
      meta: {
        title,
      },
    },
    {
      path: '/datasets/:ds_id/similarDatasets',
      name: 'DatasetDetailsSimilarDatasets',
      component: DatasetDetails,
      props: {
        activeTab: 2,
      },
      meta: {
        title,
      },
    },
    // {
    //   path: '/datasets/:ds_id/activityStream',
    //   name: 'DatasetDetailsActivityStream',
    //   component: DatasetDetails,
    //   props: {
    //     activeTab: 3,
    //   },
    //   meta: {
    //     title,
    //   },
    // },
    // {
    //   path: '/datasets/:ds_id/distributions/:dist_id',
    //   name: 'DistributionDetails',
    //   component: DistributionDetails,
    //   meta: {
    //     title,
    //   },
    // },
    {
      path: '/catalogues',
      name: 'Catalogues',
      component: Catalogues,
      meta: {
        title,
      },
      // props: { infiniteScrolling: false, pagination: true },
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        title,
      },
    },
    {
      path: '/imprint',
      name: 'Imprint',
      component: Imprint,
      title,
    },
    {
      path: '/privacypolicy',
      name: 'PrivacyPolicy',
      // TODO: implement default privacypolicy template
      // component: PrivacyPolicy,
      meta: {
        title,
      },
    },
    {
      path: '/maps',
      name: 'MapBasic',
      component: MapBasic,
      title,
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
      meta: {
        title,
      },
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
