/**
 * @author Dennis ritter
 * @description Register services and constants in the injector.
 */

// import vue-inject
import injector from 'vue-inject';

// Import glue-config.js
import { glueConfig as GLUE_CONFIG } from '../../config/user-configs';
// import impressumComp

injector.constant('baseUrl', GLUE_CONFIG.api.baseUrl);
injector.constant('gazetteerBaseUrl', GLUE_CONFIG.api.gazetteerBaseUrl);
injector.service('DatasetService', ['baseUrl'], GLUE_CONFIG.services.datasetService);
injector.service('CatalogueService', ['baseUrl'], GLUE_CONFIG.services.catalogueService);
injector.service('DistributionService', ['baseUrl'], GLUE_CONFIG.services.distributionService);
if (GLUE_CONFIG.services.mapService) injector.service('MapService', ['baseUrl'], GLUE_CONFIG.services.mapService);
if (GLUE_CONFIG.services.datastoreService) injector.service('DatastoreService', ['baseUrl'], GLUE_CONFIG.services.datastoreService);
if (GLUE_CONFIG.services.gazetteerService) injector.service('GazetteerService', ['gazetteerBaseUrl'], GLUE_CONFIG.services.gazetteerService);
