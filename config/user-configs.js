/**
 * @author Dennis ritter
 * @created 03.04.2018
 * @description Defines locations of the users config files.
 *              This is a sample file. Copy it and remove 'sample' from it's name and edit configurations.
 */

import glueConfig from '../user-config/glue-config';
import i18n from '../user-config/i18n/i18n.json';

export { glueConfig, i18n };
