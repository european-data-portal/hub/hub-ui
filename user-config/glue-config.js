/**
 * @author Dennis ritter
 * @created 25.07.17
 * @description User configuration for viaduct-ui.
 */

// Import Adapters for data requests
import datasetService from '../src/viaduct-ui-adapter-vhub/datasets';
import catalogueService from '../src/viaduct-ui-adapter-vhub/catalogues';
import distributionService from '../src/viaduct-ui-adapter-vhub/distributions';
import datastoreService from '../src/viaduct-ui-adapter-vhub/datastore';
import gazetteerService from '../src/viaduct-ui-adapter-vhub/gazetteer';

// Export Config-Object
export default {
  title: 'European Data Portal',
  api: {
    baseUrl: 'https://www.europeandataportal.eu/data/search/',
    gazetteerBaseUrl: 'https://www.europeandataportal.eu/data/search/gazetteer/',
  },
  images: {
    headerLogos: [
      {
        src: 'https://i.imgur.com/lgtG4zB.png',
        // href: 'https://my-url.de'(optional)
        // target: '_blank'(optional)
        description: 'Logo European data portal',
        height: '60px',
        width: 'auto',
      },
    ],
    footerLogos: [],
  },
  locale: 'en',
  fallbackLocale: 'en',
  services: {
    catalogueService,
    datasetService,
    distributionService,
    datastoreService,
    gazetteerService,
  },
  themes: {
    header: 'dark',
  },
  routerOptions: {
    base: '',
    mode: 'hash',
  },
  navigation: {
    topnav: {
      main: {
        home: {
          // href: 'https://link-to-external-url.com' (optional)
          // target: '_self',
          show: true,
        },
        data: {
          show: true,
        },
        maps: {
          show: false,
        },
        about: {
          show: false,
        },
        append: [
          {
            href: 'https://www.fokus.fraunhofer.de/datenschutz',
            // icon: 'rowing',
            target: '_self',
            title: 'Privacy Policy',
          },
          {
            href: 'https://www.fokus.fraunhofer.de/9663f8cb2d267d4b',
            // icon: 'rowing',
            target: '_self',
            title: 'Imprint',
          },
        ],
        icons: false,
      },
      sub: {
        privacyPolicy: {
          show: false,
          href: 'https://www.fokus.fraunhofer.de/datenschutz',
          target: '_self',
        },
        imprint: {
          show: false,
          href: 'https://www.fokus.fraunhofer.de/9663f8cb2d267d4b',
          target: '_self',
        },
      },
    },
  },
};
